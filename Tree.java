package trees_and_again_trees;

public class Tree {
	
	Tree right;
	Tree left;
	char value;
	int freq;
	
	Tree(Tree r, Tree l, int f){
		right = r;
		left = l;
		freq = f;
		value = 0;
	}
	
	Tree(char v, int f){
		value = v;
		freq = f;
		right = null;
		left = null;
	}
	
}
