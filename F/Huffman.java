package trees_and_again_trees;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;
import java.lang.*;

public class Huffman {
	public static String[] mas = new String[256];
	public static void fun(Tree tree, String string){
		if(tree.left == null){
			mas[tree.value] = string;
		} else {
			string += "0";
			fun(tree.left, string);
			string = string.substring(0, string.length() - 1);
			string += "1";
			fun(tree.right, string);
		}
	}
	
		public static void main(String[] args) {
		
		int stat[]= new int[256];
		for(int i = 0; i < 256; i++){
			stat[i] = 0;
		}
		try(FileReader reader = new FileReader("C:\\Games\\in.txt"))
        {
            int c;
            while((c = reader.read()) != -1){
            	stat[c]++;
            } 
        }
        catch(IOException ex){
             
            System.out.println(ex.getMessage());
        }
		
		Tree mas_tree[] = new Tree[256];
		for(int i = 0; i < 256; i++){
			mas_tree[i] = new Tree((char)i, stat[i]);
		}
		int min1, min2, index1, index2;
		min1 = min2 = index1 = index2 = 0;
		int counter = 2;
		while(counter >= 2){
		for(int i = 0; i < 256; i++){
			if(stat[i] != 0){
				min1 = stat[i];
				index1 = i;
				break;
			}
		}
		for(int i = index1 + 1; i < 256; i++){
			if(stat[i] != 0){
				min2 = stat[i];
				index2 = i;
				break;
			}
		}
		for(int i = 0; i < 256; i++){
			if(stat[i] <= min1 && stat[i] != 0){
				min1 = stat[i];
				index1 = i;
			}
		}
		for(int i = 0; i < 256; i++){
			if(stat[i] <= min2 && stat[i] != 0 && i != index1){
				min2 = stat[i];
				index2 = i;
			}
		}
		mas_tree[index1] = new Tree(mas_tree[index1], mas_tree[index2], min1 + min2);
		stat[index1] += stat[index2];
		stat[index2] = 0;
		counter = 0;
		for(int i = 0; i < 256; i++){
			if(stat[i] != 0){
				counter++;
			}
		}
		}
		int k = 0;
		for(int i = 0; i < 256; i++){
			if(stat[i] != 0){
				k = i;
				break;
			}
		}
		Tree tree = mas_tree[k];   
		fun(tree, "");
		
		for(int i = 0; i < 256; i++){
			if(mas[i] != null){
				System.out.println(mas[i]);
			}
		}
		
		try(FileWriter writer = new FileWriter("C:\\Games\\out.txt"))
        {
            
            try(FileReader nreader = new FileReader("C:\\Games\\in.txt"))
            {
                int c;
                while((c = nreader.read()) != -1){
                	writer.write(mas[c]);
                } 
            }
            catch(IOException ex){
                 
                System.out.println(ex.getMessage());
            }
            writer.flush();
        }
        catch(IOException ex){
             
            System.out.println(ex.getMessage());
        } 
		
	}

}
