package проги;

import java.math.*;

public class Am_Bn extends Thread{
    char whoAmI;
    static char whoIsNext = 'A';
    final Object synchro;
    static int M;
    static int N;
    static int counter = 0;

    Am_Bn(char whoami, Object sync, int m, int n){
        whoAmI = whoami;
        synchro = sync;
        M = m;
        N = n;
    }

    void dodo(){
        synchronized(synchro){
            for(int i = 0; i < Math.min(M, N); i++){
                System.out.print(whoAmI);
                counter++;
                if(counter == 2*N || counter == 2*M){
                    if(M > N){
                        for(int j = 0; j < M - N; j++){
                            System.out.print('A');
                        }
                        synchro.notifyAll();
                        break;
                    } else if(M < N){
                        for(int j = 0; j < N - M; j++){
                            System.out.print('B');
                        }
                        synchro.notifyAll();
                        break;
                    } else {
                        synchro.notifyAll();
                        break;
                    }
                } else {
                    synchro.notifyAll();
                    try{
                        synchro.wait();
                    } catch (InterruptedException e){
                        e.printStackTrace();
                    }
                }}
        }
    }

    @Override
    public void run(){
        dodo();
        stop();
    }

}