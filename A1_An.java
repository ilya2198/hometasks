package проги;

public class A1_An extends Thread{

    int numberOfChars;
    char[] mas;
    char whoAmI;
    static char whoIsNext = 'A';
    static int current_i = 0;
    final Object synchro;

    A1_An(char whoami, Object sync, char[] massive, int numberofchars){
        mas = new char[numberofchars];
        for(int i = 0; i < numberofchars; i++){
            mas[i] = massive[i];
        }
        whoAmI = whoami;
        synchro = sync;
        numberOfChars = numberofchars;
    }
    void dodo(){
        synchronized(synchro){
            for(int i = 0; i < 15; i++){
                if(whoIsNext == whoAmI){
                    System.out.print(whoAmI);
                    if(current_i + 1 < numberOfChars){
                        current_i++;
                        whoIsNext = mas[current_i];
                    } else {
                        current_i = 0;
                        whoIsNext = mas[0];
                    }
                    synchro.notifyAll();
                    try{
                        synchro.wait();
                    } catch (InterruptedException e){
                        e.printStackTrace();
                    }
                } else {
                    i--;
                    synchro.notifyAll();
                    try{
                        synchro.wait();
                    } catch (InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }
            synchro.notifyAll();
        }
    }

    @Override
    public void run(){
        dodo();
        stop();
    }


}
